package main;

import org.lwjgl.input.Keyboard;

/**
 * Created by marcusbrooks on 26/02/15.
 */
public class InputHandler {

    public void pollInput() {

        while (Keyboard.next()) {
            if (Keyboard.getEventKeyState()) {//KeyDown
                if(Keyboard.getEventKey() == Keyboard.KEY_TAB){
                    Main.currentInputType.toggle();
                } else if (Keyboard.getEventKey() == Keyboard.KEY_1) {
                    Main.inputType.SPHERE.update();
                } else if (Keyboard.getEventKey() == Keyboard.KEY_2) {
                    Main.inputType.SINE_WAVE.update();
                } else if (Keyboard.getEventKey() == Keyboard.KEY_0) {
                    Main.currentSmoothType.toggle();
                } else if (Keyboard.getEventKey() == Keyboard.KEY_RBRACKET) {
                    Main.incrementChunkSize(10);
                } else if (Keyboard.getEventKey() == Keyboard.KEY_LBRACKET) {
                    Main.decrementChunkSize(10);
                } else if (Keyboard.getEventKey() == Keyboard.KEY_9) {
                    Main.toggleWireframe();
                } else if (Keyboard.getEventKey() == Keyboard.KEY_O) {
                    Main.currentSmoothType.iter++;
                    Main.currentRenderType.update();
                } else if (Keyboard.getEventKey() == Keyboard.KEY_P) {
                    Main.currentSmoothType.iter--;
                    Main.currentRenderType.update();
                } else if (Keyboard.getEventKey() == Keyboard.KEY_C) {
                    Main.currentColourType.toggle();
                } else if (Keyboard.getEventKey() == Keyboard.KEY_H){
                    Global.USE_COLOR_HACK = !Global.USE_COLOR_HACK;
                    Main.currentRenderType.update();
                } else if(Keyboard.getEventKey() == Keyboard.KEY_T){
                    Main.target();
                } else if(Keyboard.getEventKey() == Keyboard.KEY_RETURN){
                    Main.modifyChunk();
                } else if(Keyboard.getEventKey() == Keyboard.KEY_L){
                    Main.newLight();
                }

            }

        }


    }
}