package main;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.util.vector.Vector3f;
import render.Camera;
import render.Renderer;
import util.Util;
import voxel.ScalarData;
import voxel.SurfaceNet;
import voxel.Voxel;

import java.util.Random;
import java.util.logging.*;

public class Main {

    private static final Logger debug = Logger.getLogger(Main.class.getName());

    public enum inputType {
        SPHERE {
            @Override
            public void toggle() {
                SPHERES.update();
            }

            @Override
            public inputType update() {
                currentInputType = this;
                chunk = ScalarData.createSphere();
                currentRenderType.update();
                return this;
            }
        },
        SPHERES {
            @Override
            public void toggle() {
                SINE_WAVE.update();
            }

            @Override
            public inputType update() {
                currentInputType = this;
                chunk = ScalarData.createSpheres(10);
                currentRenderType.update();
                return this;
            }
        },
        SINE_WAVE {
            @Override
            public void toggle() {
                HEIGHT_MAP.update();
            }

            @Override
            public inputType update() {
                currentInputType = this;
                chunk = ScalarData.createSineWaves();
                currentRenderType.update();
                return this;
            }
        },
        HEIGHT_MAP {
            @Override
            public void toggle() {
                PERLIN_NOISE.update();
            }

            @Override
            public inputType update() {
                currentInputType = this;
                chunk = ScalarData.createHeightMap();
                currentRenderType.update();
                return this;
            }
        },
        PERLIN_NOISE {
            @Override
            public void toggle() {
                PLANET.update();
            }

            @Override
            public inputType update() {
                currentInputType = this;
                chunk = ScalarData.createPerlinNoise();
                currentRenderType.update();
                return this;
            }
        },
        PLANET {
            @Override
            public void toggle() {
                SPHERE.update();
            }

            @Override
            public inputType update() {
                currentInputType = this;
                chunk = ScalarData.createAsteroid();
                currentRenderType.update();
                return this;
            }
        };
        public abstract inputType update();
        public abstract void toggle();
    }

    public enum smoothType {
        EDGE_SMOOTHING {
            @Override
            public smoothType update() {
                SurfaceNet.smoothSurfaceByEdgeMidpoints(iter);
                return this;
            }

            @Override
            public void toggle() {
                reset();
                currentSmoothType = smoothType.NODE_SMOOTHING;
                currentRenderType.update();
            }
        },
        NODE_SMOOTHING {
            @Override
            public smoothType update() {

                SurfaceNet.smoothSurfaceIteratively(iter);
                return this;
            }
            @Override
            public void toggle() {
                reset();
                currentSmoothType = smoothType.NO_SMOOTHING;
                currentRenderType.update();
            }
        },
        NO_SMOOTHING {
            @Override
            public smoothType update() {
                return null;
            }
            @Override
            public void toggle() {
                reset();
                currentSmoothType = smoothType.EDGE_SMOOTHING;
                currentRenderType.update();
            }

            @Override
            public int getIter() {
                return 0;
            }
        };

        int iter = 10;
        public abstract smoothType update();
        public abstract void toggle();
        public int getIter(){ return iter; };
        public void reset(){ iter = 1; }
    }

    public enum renderType {
        SURFACE_NET {
            @Override
            public renderType update() {
                SurfaceNet.createSurfaceNodes(chunk, Global.CHUNK_SIZE);
                currentSmoothType.update();
                renderer.createSurfaceNet(chunk);
                return this;
            }
        },
        BOXEL {
            @Override
            public renderType update() {
                renderer.createBoxels(chunk);

                return this;
            }
        };

        public abstract renderType update();
    }

    public enum colourType {
        RGB {
            @Override
            public void toggle() {
                currentColourType = colourType.NORMAL;
                currentRenderType.update();
            }
        },
        NORMAL{
            @Override
            public void toggle() {
                currentColourType = colourType.DIFFUSE;
                currentRenderType.update();
            }
        },
        DIFFUSE {
            @Override
            public void toggle() {
                currentColourType = colourType.RGB;
                currentRenderType.update();
            }
        };
        public abstract void toggle();
    }


    public static inputType currentInputType = inputType.SINE_WAVE;
    public static smoothType currentSmoothType = smoothType.NO_SMOOTHING;
    public static renderType currentRenderType = renderType.SURFACE_NET;
    public static colourType currentColourType = colourType.RGB;

    private static Voxel[][][] chunk = new Voxel[Global.CHUNK_SIZE][Global.CHUNK_SIZE][Global.CHUNK_SIZE];

    private InputHandler input;
    private static Renderer renderer;
    public static boolean exit = false;
    public void start() {
        try {
            Display.setDisplayMode(new DisplayMode(Global.WIDTH, Global.HEIGHT));
            Display.setTitle("Voxel Test");
            Display.create();
            Display.setVSyncEnabled(true);
            input = new InputHandler();
            renderer = new Renderer();
            Camera.create();
        } catch (LWJGLException e) {
            e.printStackTrace();
            System.exit(0);
        }

        //Initialise openGL
        renderer.initGL();
        //Load textures/fonts
        renderer.loadAssets();

        //Create the edge table
        SurfaceNet.initTables();

        //Fill the chunk with voxel data
        currentInputType.update();

        //Gameloop
        while (!exit) {
            Display.update();
            int delta = getDelta();

            //Check for close requests from outside of the application
            if( Display.isCloseRequested() ) {
                exit = true;
            }

            // The window is in the foreground
            else if (Display.isActive()) {
                update(delta);
                renderer.render(delta);
            }

            //Window is not focused
            else {
                update(0);

                // Only bother rendering if the window is visible or dirty
                if (Display.isVisible() || Display.isDirty()) {
                    renderer.render(delta);
                }
            }

            //Limit the frames
            Display.sync( Global.FRAMERATE );

        }

        //After exit, dispose of everything
        dispose();
    }

    private void update(float delta) {
        input.pollInput();
        Camera.acceptInput(delta);
        Camera.apply();
    }

    private void dispose(){
        Display.destroy();
        renderer.dispose();
    }

    public static long getTime() {
        return (Sys.getTime() * 1000) / Sys.getTimerResolution();
    }

    long lastTime = 0;
    private int getDelta() {
        if(lastTime == 0)
            lastTime = getTime();

        long currentTime = getTime();
        int delta = (int)(currentTime - lastTime);
        lastTime = getTime();
        return delta;
    }

    public static void incrementChunkSize(int inc){
        Global.CHUNK_SIZE += inc;
        currentInputType.update();
    }

    public static void decrementChunkSize(int dec){
        Global.CHUNK_SIZE -= dec;
        currentInputType.update();
    }

    public static void newLight() {
        Util.newLight();
        currentRenderType.update();
    }

    public static void toggleWireframe() {
        renderer.toggleWireframe();
    }

    public static void modifyChunk(){
        Random rand = new Random();
        for (int i = 0; i < 1; i++) {
            //chunk = ScalarData.modifyChunk(chunk, rand.nextInt(Global.CHUNK_SIZE), rand.nextInt(Global.CHUNK_SIZE), rand.nextInt(Global.CHUNK_SIZE));
            chunk = ScalarData.modifySurface(chunk, rand.nextInt(Global.CHUNK_SIZE), rand.nextInt(Global.CHUNK_SIZE));
            //chunk = ScalarData.modifyChunk(chunk);
        }
        currentRenderType.update();
    }

    public static void target() {
        Camera.setRotation(new Vector3f(Camera.getX()-(Global.CHUNK_SIZE / 2),Camera.getY()-(Global.CHUNK_SIZE / 2),Camera.getZ()-(Global.CHUNK_SIZE / 2)));
    }

    /**
     * Creates a logger to print to console
     */
    private static void initLogging() {
        /**
         * Override consoleHandler's publish method so not all console logs go through System.err
         */
        Handler consoleHandler = new Handler() {
            @Override
            public void publish(LogRecord record) {
                if (getFormatter() == null) {
                    setFormatter(new SimpleFormatter());
                }

                try {
                    String message = getFormatter().format(record);
                    if (record.getLevel().intValue() >= Level.WARNING.intValue()) {
                        System.err.write(message.getBytes());
                    } else {
                        System.out.write(message.getBytes());
                    }
                } catch (Exception exception) {
                    reportError(null, exception, ErrorManager.FORMAT_FAILURE);
                    return;
                }

            }

            @Override
            public void close() throws SecurityException {
            }

            @Override
            public void flush() {
            }
        };

        debug.setLevel(Level.OFF);
        consoleHandler.setLevel(Level.ALL);
        debug.setUseParentHandlers(false);
        debug.addHandler(consoleHandler);

    }

    public static void main(String[] argv) {
        initLogging();

        debug.log(Level.INFO, "Running LWJGL " + Sys.getVersion());

        new Main().start();

        debug.log(Level.WARNING, "Thread closed. Terminating.");
    }
}