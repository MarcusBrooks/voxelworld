package main;

/**
 * Created by marcusbrooks on 26/02/15.
 */
public class Global {
    /** Desired frame time */
    public static final int FRAMERATE = 60;

    public static int CHUNK_SIZE = 64;

    public static final int HEIGHT = 720;
    public static final int WIDTH = 1280;

    public static final int CAMERA_SPEED = 1;
    public static final int CAMERA_ROTATE = 3;

    public static boolean USE_PRECALC_QUAD_TABLE = true;
    public static final boolean USE_SIMPLE_RENDERING = true;

    public static boolean USE_COLOR_HACK = true;

}
