package test;

import main.Global;
import org.junit.Assert;
import org.junit.Test;
import voxel.Node;
import voxel.ScalarData;
import voxel.SurfaceNet;
import voxel.Voxel;

public class SurfaceNetTest {

/*
        Deprecated as surfaceNodes can now have null values
 */
    @Test
    public void testCreateSurfaceNodes(){
        SurfaceNet.initTables();

        Voxel[][][] chunk = ScalarData.createSphere();

        int iter = 10;




        for (int i = 0; i < 10; i++) {
            SurfaceNet.createSurfaceNodes(chunk, Global.CHUNK_SIZE);
        }

        Global.USE_PRECALC_QUAD_TABLE = false;

        long start2 = System.nanoTime();
        for (int i = 0; i < iter; i++) {
            SurfaceNet.createSurfaceNodes(chunk, Global.CHUNK_SIZE);
        }
        long end2 = System.nanoTime();
        long avgTime2 = (end2 - start2) / iter;

        System.out.println("Average time: createSurfaceNodes() - Precalc = false " + avgTime2/1e6);


        Global.USE_PRECALC_QUAD_TABLE = true;


        long start = System.nanoTime();
        for (int i = 0; i < iter; i++) {
            SurfaceNet.createSurfaceNodes(chunk, Global.CHUNK_SIZE);
        }
        long end = System.nanoTime();
        long avgTime = (end - start) / iter;


        System.out.println("Average time: createSurfaceNodes() - Precalc = true " + avgTime/1e6);
        System.out.println("Average time: createSurfaceNodes() - Precalc = false " + avgTime2/1e6);

    }

    @Test
    public void testCheckSurfaceNode() throws Exception {
        Voxel[][][] t1 = new Voxel[2][2][2];

        //Create an empty chunk, representing a search space outside of the voxel mass.
        for (int x = 0; x < 2; x++) {
            for (int y = 0; y < 2; y++) {
                for (int z = 0; z < 2; z++) {
                    t1[x][y][z] = new Voxel(Voxel.Type.EMPTY);
                    t1[x][y][z].setActive(false);
                }
            }
        }
        Assert.assertTrue(SurfaceNet.checkSurfaceNode(t1) == 0 || SurfaceNet.checkSurfaceNode(t1) == 0xff);


        Voxel[][][] t2 = new Voxel[2][2][2];

        //Create an empty chunk, representing a search space completely inside of the voxel mass.
        for (int x = 0; x < 2; x++) {
            for (int y = 0; y < 2; y++) {
                for (int z = 0; z < 2; z++) {
                    t2[x][y][z] = new Voxel(Voxel.Type.FILLED);
                    t2[x][y][z].setActive(true);
                }
            }
        }

        Assert.assertTrue(SurfaceNet.checkSurfaceNode(t2) == 0x00 || SurfaceNet.checkSurfaceNode(t2) == 0xff);

        Voxel[][][] t3 = new Voxel[2][2][2];

        //Change 1 Voxel to be active, indicating that the search space lies on the surface of the voxel mass
        for (int x = 0; x < 2; x++) {
            for (int y = 0; y < 2; y++) {
                for (int z = 0; z < 2; z++) {
                    t3[x][y][z] = new Voxel(Voxel.Type.EMPTY);


                    if (x == 1) {
                        t3[x][y][z] = new Voxel(Voxel.Type.FILLED);
                    }
                }
            }
        }

        Assert.assertFalse(SurfaceNet.checkSurfaceNode(t3) == 0 || SurfaceNet.checkSurfaceNode(t3) == 0xff);


        Voxel empty = new Voxel(Voxel.Type.EMPTY);
        Voxel filled = new Voxel(Voxel.Type.FILLED);

        Voxel[][][] t4 = {{{filled, filled},
                        {filled, filled}},

                        {{empty, filled},
                         {empty, filled}}};

        Assert.assertTrue(175 == SurfaceNet.checkSurfaceNode(t4));


        Voxel[][][] t5 = {{{empty, filled},
                        {filled, filled}},

                        {{empty, filled},
                        {filled, filled}}};

        Assert.assertTrue(238 == SurfaceNet.checkSurfaceNode(t5));
    }
}

