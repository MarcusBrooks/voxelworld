package util;

import main.Global;
import main.Main;
import org.lwjgl.util.vector.Vector3f;

/**
 * Created by marcusbrooks on 27/02/15.
 */
public class Util {
    static Vector3f lightDir = new Vector3f(0,-1,0);

    public static void newLight(){
        lightDir = new Vector3f(Rand.nextFloat(), -1, Rand.nextFloat());
        lightDir.normalise(lightDir);
    }

    public static Vector3f getColor(Vector3f p1, Vector3f p2, Vector3f p3, Vector3f p4, int index){

        if(Main.currentColourType == Main.colourType.NORMAL){
            if(Global.USE_COLOR_HACK) {
                Vector3f[] colors = new Vector3f[]{getNormal(p1, p2, p3), getNormal(p2, p3, p4), getNormal(p3, p4, p1), getNormal(p4, p1, p2)};

                Vector3f color = new Vector3f(colors[0].getX() + colors[1].getX() + colors[2].getX() + colors[3].getX() / 4,
                        colors[0].getY() + colors[1].getY() + colors[2].getY() + colors[3].getY() / 4,
                        colors[0].getZ() + colors[1].getZ() + colors[2].getZ() + colors[3].getZ() / 4);

                color = color.normalise(color);
                if(true) {
                    float lastDot = 0;
                    int j = 0;
                    for (int i = 0; i < 4; i++) {
                        float dot = Vector3f.dot(color, colors[i]);
                        if (dot == 1) {
                            return color;
                        } else {
                            if (dot >
                                    lastDot) {
                                lastDot = dot;
                                j = i;
                            }
                        }
                    }
                    if (lastDot == 0) {
                        return color;
                    } else {
                        return colors[j];
                    }
                } else {
                    return color;
                }
            } else {
                return getNormal(p1, p2, p3);
            }
        } else if(Main.currentColourType == Main.colourType.DIFFUSE) {

            if(Global.USE_COLOR_HACK) {

                float diffuse1 = Math.abs(Vector3f.dot(lightDir, getNormal(p1, p2, p3)));
                float diffuse2 = Math.abs(Vector3f.dot(lightDir, getNormal(p2, p3, p4)));
                float diffuse3 = Math.abs(Vector3f.dot(lightDir, getNormal(p3, p4, p1)));
                float diffuse4 = Math.abs(Vector3f.dot(lightDir, getNormal(p4, p1, p2)));

                float avgDiffuse = (diffuse1+diffuse2+diffuse3+diffuse4)/4;
                return new Vector3f(0.6f*avgDiffuse, 0.7f * avgDiffuse,0.52f*avgDiffuse);

            } else {
                float diffuse = Math.abs(Vector3f.dot(lightDir, getNormal(p1 ,p2, p3)));
                return new Vector3f(0.6f*diffuse, 0.7f * diffuse,0.52f*diffuse);
            }

        } else if(index == 0){
            return new Vector3f(0,0,1);
        } else if (index == 1){
            return new Vector3f(0,1,0);
        } else if(index == 2) {
            return new Vector3f(1,0,0);
        } else if(index == 3) {
            return new Vector3f(1,1,0);
        } else if (index == 4) {
            return new Vector3f(1,0,1);
        } else if (index == 5) {
            return new Vector3f(0,1,1);
        } else {
            return new Vector3f(1,1,1);
        }
    }

    /**
     * Returns the normal of a triangle
     * @param p1 Vector3f
     * @param p2 Vector3f
     * @param p3 Vector3f
     * @return Vector3f
     */
    public static Vector3f getNormal(Vector3f p1, Vector3f p2, Vector3f p3) {

        //Create normal vector we are going to output.
        Vector3f output = new Vector3f();

        //Calculate vectors used for creating normal (these are the edges of the triangle).
        Vector3f calU = new Vector3f(p2.x - p1.x, p2.y - p1.y, p2.z - p1.z);
        Vector3f calV = new Vector3f(p3.x - p1.x, p3.y - p1.y, p3.z - p1.z);

        //The output vector is equal to the cross products of the two edges of the triangle
        output.x = Math.abs(calU.y * calV.z - calU.z * calV.y);
        output.y = Math.abs(calU.z * calV.x - calU.x * calV.z);
        output.z = Math.abs(calU.x * calV.y - calU.y * calV.x);

        //Return the resulting vector.
        return output.normalise(output);


    }

    /**
     * Creates a height map from layers of perlin noise at different frequencies.
     * @return double[][]
     */
    public static double[][] createNewHeightMap() {
        PerlinNoise perlin = new PerlinNoise(123);
        double noise[][] = new double[Global.CHUNK_SIZE][Global.CHUNK_SIZE];

        for (int x = 0; x < Global.CHUNK_SIZE; x++) {
            for (int y = 0; y < Global.CHUNK_SIZE; y++) {
                double v = 0;
                for (int i = 2; i <= 256; i = i * i) {
                    double n = perlin.noise2(i * x / (float) Global.CHUNK_SIZE, i * y / (float) Global.CHUNK_SIZE);
                    v += n / i;
                }
                noise[x][y] = v;
            }
        }
        return noise;
    }

}
