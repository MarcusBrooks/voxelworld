package render;

import main.Global;
import main.Main;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBVertexBufferObject;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.util.ResourceLoader;
import voxel.Boxel;
import voxel.ScalarData;
import voxel.SurfaceNet;
import voxel.Voxel;

import java.awt.*;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.logging.Logger;

import static org.lwjgl.opengl.ARBVertexBufferObject.*;
import static org.lwjgl.opengl.GL11.*;

/**
 * Created by marcusbrooks on 27/02/15.
 */
public class Renderer {
    private static final Logger debug = Logger.getLogger(Renderer.class.getName());

    public static boolean wireframeVisible = false;
    public static int numberOfQuads = 0;

    TrueTypeFont font;
    TrueTypeFont font2;

    public void initGL() {
        glViewport(0, 0, Global.WIDTH, Global.HEIGHT); // Reset The Current Viewport
        glMatrixMode(GL11.GL_PROJECTION); // Select The Projection Matrix
        glLoadIdentity(); // Reset The Projection Matrix
        GLU.gluPerspective(45.0f, ((float) Global.WIDTH / (float) Global.HEIGHT), 0.1f, 1000.0f); // Calculate The Aspect Ratio Of The Window
        glMatrixMode(GL11.GL_MODELVIEW); // Select The Modelview Matrix
        glLoadIdentity(); // Reset The Modelview Matrix

        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);

        glShadeModel(GL11.GL_SMOOTH); // Enables Smooth Shading
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Black Background
        glClearDepth(1.0f); // Depth Buffer Setup
        glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        glEnable(GL11.GL_DEPTH_TEST); // Enables Depth Testing
        glDepthFunc(GL11.GL_LEQUAL); // The Type Of Depth Test To Do
        glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST); //Perspective Calculations
    }

    public void loadAssets() {
        // load a default java font
        Font awtFont = new Font("Times New Roman", Font.BOLD, 24);
        font = new TrueTypeFont(awtFont, true);

        // load font from a .ttf file
        try {
            InputStream inputStream	= ResourceLoader.getResourceAsStream("assets/fonts/AgencyGothic.ttf");

            Font awtFont2 = Font.createFont(Font.TRUETYPE_FONT, inputStream);
            awtFont2 = awtFont2.deriveFont(24f); // set font size
            font2 = new TrueTypeFont(awtFont2, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void render(int delta) {
        glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

        if(wireframeVisible) {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); //Wireframe ON
        } else {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL ); //Wireframe OFF
        }
        if(!Global.USE_SIMPLE_RENDERING) {
            glEnable(GL_CULL_FACE);
        }

        glVertexPointer(3, GL_FLOAT, /* stride */(3 * 2) << 2, /* offset */0L << 2); // float at index 0
        glColorPointer(3, GL_FLOAT, /* stride */(3 * 2) << 2, /* offset */(3 * 1) << 2); // float at index 3

        glDrawArrays(GL_QUADS, 0, 4 * numberOfQuads * 2/* elements */);


        renderHud(delta);

    }

    public void renderHud(int delta){
        calculateFps(delta);

        //Set the camera to Orthographic to display the text within the bounds and facing the camera
        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();
        glOrtho(0.0, Global.WIDTH, Global.HEIGHT, 0.0, -1.0, 10.0);
        glMatrixMode(GL_MODELVIEW);

        glLoadIdentity();
        glDisable(GL_CULL_FACE);

        glClear(GL_DEPTH_BUFFER_BIT);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        font2.drawString(0, 0, "Frame Rate:  " + fps, Color.green);




        font2.drawString(0, Global.HEIGHT - 170, "Input: " + Main.currentInputType.toString(), Color.green);
        font2.drawString(0, Global.HEIGHT - 146,  Main.currentSmoothType.toString(), Color.green);
        font2.drawString(0, Global.HEIGHT - 122, "Smoothing iterations: " + Main.currentSmoothType.getIter(), Color.green);
        font2.drawString(0, Global.HEIGHT - 98f, "Chunk Size: " + Global.CHUNK_SIZE + " ("+(int)Math.pow(Global.CHUNK_SIZE, 3) + ")", Color.green);
        font2.drawString(0, Global.HEIGHT - 74f, "Filled Voxels: "+ ScalarData.currentFilledVoxels, Color.green);
        font2.drawString(0, Global.HEIGHT - 48f, "Empty Voxels: "+ ScalarData.currentEmptyVoxels, Color.green);
        font2.drawString(0, Global.HEIGHT - 24f, "Quads rendered: " + numberOfQuads, Color.green);

        glBlendFunc(GL_ONE, GL_ZERO);


        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
        glMatrixMode(GL_MODELVIEW);
    }

    int time = 0;
    int fpsCount = 0;
    int fps = 0;
    public void calculateFps(int delta){
        time += delta;
        fpsCount++;
        if(time >= 1000){
            fps = fpsCount;
            fpsCount = 0;
            time = 0;
        }
    }

    public void createSurfaceNet(Voxel[][][] chunk) {
        numberOfQuads = Boxel.countQuads(chunk);

        IntBuffer ib = BufferUtils.createIntBuffer(1);

        glGenBuffersARB(ib);
        int vcHandle = ib.get(0);

        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_COLOR_ARRAY);

        glBindBufferARB(GL_ARRAY_BUFFER_ARB, vcHandle);
        glBufferDataARB(GL_ARRAY_BUFFER_ARB, ((12 + 3) * numberOfQuads * 4) << 2, GL_STATIC_DRAW_ARB);


        ByteBuffer dataBuffer = glMapBufferARB(GL_ARRAY_BUFFER_ARB, ARBVertexBufferObject.GL_WRITE_ONLY_ARB, ((12 + 3) * numberOfQuads * 4) << 2, null);

        // create geometry buffer (both vertex and color)
        FloatBuffer vcBuffer = dataBuffer.order(ByteOrder.nativeOrder()).asFloatBuffer();

        SurfaceNet.meshSurfaceNodes(vcBuffer);

        vcBuffer.flip();
        glUnmapBufferARB(GL_ARRAY_BUFFER_ARB);
    }

    public void createBoxels(Voxel[][][] chunk){
        numberOfQuads = Boxel.countQuads(chunk);

        IntBuffer ib = BufferUtils.createIntBuffer(1);

        glGenBuffersARB(ib);
        int vcHandle = ib.get(0);

        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_COLOR_ARRAY);

        glBindBufferARB(GL_ARRAY_BUFFER_ARB, vcHandle);
        glBufferDataARB(GL_ARRAY_BUFFER_ARB, ((12 + 3) * numberOfQuads * 4) << 2, GL_STATIC_DRAW_ARB);


        ByteBuffer dataBuffer = glMapBufferARB(GL_ARRAY_BUFFER_ARB, ARBVertexBufferObject.GL_WRITE_ONLY_ARB, ((12 + 3) * numberOfQuads * 4) << 2, null);

        // create geometry buffer (both vertex and color)
        FloatBuffer vcBuffer = dataBuffer.order(ByteOrder.nativeOrder()).asFloatBuffer();

//        SurfaceNet.meshSurfaceNodes(vcBuffer);

        for (int x = 0; x < Global.CHUNK_SIZE; x++) {
            for (int y = 0; y < Global.CHUNK_SIZE; y++) {
                for (int z = 0; z < Global.CHUNK_SIZE; z++) {
                    if (!chunk[x][y][z].isActive()) {
                        continue; //Skip current cube if not active
                    }

                    boolean xNegative = true;
                    if (x > 0) {
                        xNegative = !chunk[x - 1][y][z].isActive();
                    }

                    boolean xPositive = true;
                    if (x < Global.CHUNK_SIZE - 1) {
                        xPositive = !chunk[x + 1][y][z].isActive();
                    }

                    boolean yNegative = true;
                    if (y > 0) {
                        yNegative = !chunk[x][y - 1][z].isActive();
                    }

                    boolean yPositive = true;
                    if (y < Global.CHUNK_SIZE - 1) {
                        yPositive = !chunk[x][y + 1][z].isActive();
                    }

                    boolean zNegative = true;
                    if (z > 0) {
                        zNegative = !chunk[x][y][z - 1].isActive();
                    }

                    boolean zPositive = true;
                    if (z < Global.CHUNK_SIZE - 1) {
                        zPositive = !chunk[x][y][z + 1].isActive();
                    }

                    Boxel.createCube(vcBuffer, x, y, z, zPositive, zNegative, xNegative, xPositive, yPositive, yNegative);
                }
            }
        }

        vcBuffer.flip();
        glUnmapBufferARB(GL_ARRAY_BUFFER_ARB);
    }

    public void toggleWireframe(){
        wireframeVisible = !wireframeVisible;
    }

    public void dispose(){

    }
}
