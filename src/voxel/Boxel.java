package voxel;

import main.Global;
import util.Util;
import org.lwjgl.util.vector.Vector3f;

import java.nio.FloatBuffer;

/**
 * Created by marcusbrooks on 27/02/15.
 */
public class Boxel {

    private static final float size = 0.5f;

    public static int countQuads(Voxel[][][] chunk) {
        int quads = 0;

        for (int x = 0; x < Global.CHUNK_SIZE; x++) {
            for (int y = 0; y < Global.CHUNK_SIZE; y++) {
                for (int z = 0; z < Global.CHUNK_SIZE; z++) {
                    if (!chunk[x][y][z].isActive()) {
                        continue; //Skip current cube if not active
                    }

                    boolean xNegative = true;
                    if (x > 0) {
                        xNegative = !chunk[x - 1][y][z].isActive();
                        if (xNegative) {
                            quads++;
                        }
                    }

                    boolean xPositive = true;
                    if (x < Global.CHUNK_SIZE - 1) {
                        xPositive = !chunk[x + 1][y][z].isActive();
                        if (xPositive) {
                            quads++;
                        }
                    }

                    boolean yNegative = true;
                    if (y > 0) {
                        yNegative = !chunk[x][y - 1][z].isActive();
                        if (yNegative) {
                            quads++;
                        }
                    }

                    boolean yPositive = true;
                    if (y < Global.CHUNK_SIZE - 1) {
                        yPositive = !chunk[x][y + 1][z].isActive();
                        if (yPositive) {
                            quads++;
                        }
                    }

                    boolean zNegative = true;
                    if (z > 0) {
                        zNegative = !chunk[x][y][z - 1].isActive();
                        if (zNegative) {
                            quads++;
                        }
                    }

                    boolean zPositive = true;
                    if (z < Global.CHUNK_SIZE - 1) {
                        zPositive = !chunk[x][y][z + 1].isActive();
                        if (zPositive) {
                            quads++;
                        }
                    }

                    //createCube(vcBuffer, x, y, z, zNegative, zPositive, xPositive, xNegative, yNegative, yPositive);

                }
            }
        }
        return quads;
    }

    public static void createCube(FloatBuffer vcBuffer, float x, float y, float z) {
        createCube(vcBuffer, x, y, z, true, true, true, true, true, true);
    }

    public static void createCube(FloatBuffer vcBuffer, float x, float y, float z, boolean front, boolean back, boolean left, boolean right, boolean top, boolean bottom) {
try {
    if (front) {
        addFrontFace(vcBuffer, x, y, z);
    }
    if (back) {
        addBackFace(vcBuffer, x, y, z);
    }
    if (left) {
        addLeftFace(vcBuffer, x, y, z);
    }
    if (right) {
        addRightFace(vcBuffer, x, y, z);
    }
    if (top) {
        addTopFace(vcBuffer, x, y, z);
    }
    if (bottom) {
        addBottomFace(vcBuffer, x, y, z);
    }
} catch(Exception e){
    System.out.println(e.getMessage());
}
    }

    public static void addFrontFace(FloatBuffer vcBuffer, float x, float y, float z) {

        Vector3f normal = Util.getNormal(new Vector3f(x - size, y - size, z + size), new Vector3f(x + size, y - size, z + size), new Vector3f(x + size, y + size, z + size));

        vcBuffer.put(x - size).put(y - size).put(z + size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x + size).put(y - size).put(z + size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x + size).put(y + size).put(z + size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x - size).put(y + size).put(z + size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c
    }

    public static void addBackFace(FloatBuffer vcBuffer, float x, float y, float z) {

        Vector3f normal = Util.getNormal(new Vector3f(x - size, y - size, z - size), new Vector3f(x - size, y + size, z - size), new Vector3f(x + size, y + size, z - size));

        vcBuffer.put(x - size).put(y - size).put(z - size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x - size).put(y + size).put(z - size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x + size).put(y + size).put(z - size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x + size).put(y - size).put(z - size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c
    }

    public static void addLeftFace(FloatBuffer vcBuffer, float x, float y, float z) {

        Vector3f normal = Util.getNormal(new Vector3f(x - size, y - size, z + size), new Vector3f(x - size, y + size, z + size), new Vector3f(x - size, y + size, z - size));

        vcBuffer.put(x - size).put(y - size).put(z + size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x - size).put(y + size).put(z + size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x - size).put(y + size).put(z - size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x - size).put(y - size).put(z - size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c
    }


    public static void addRightFace(FloatBuffer vcBuffer, float x, float y, float z) {
        Vector3f normal = Util.getNormal(new Vector3f(x + size, y - size, z - size), new Vector3f(x + size, y + size, z - size), new Vector3f(x + size, y + size, z + size));

        vcBuffer.put(x + size).put(y - size).put(z - size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x + size).put(y + size).put(z - size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x + size).put(y + size).put(z + size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x + size).put(y - size).put(z + size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

    }

    public static void addTopFace(FloatBuffer vcBuffer, float x, float y, float z) {
        Vector3f normal = Util.getNormal(new Vector3f(x - size, y + size, z + size), new Vector3f(x + size, y + size, z + size), new Vector3f(x + size, y + size, z - size));

        vcBuffer.put(x - size).put(y + size).put(z + size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x + size).put(y + size).put(z + size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x + size).put(y + size).put(z - size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x - size).put(y + size).put(z - size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

    }


    public static void addBottomFace(FloatBuffer vcBuffer, float x, float y, float z) {
        Vector3f normal = Util.getNormal(new Vector3f(x - size, y - size, z + size), new Vector3f(x - size, y - size, z - size), new Vector3f(x + size, y - size, z - size));

        vcBuffer.put(x - size).put(y - size).put(z + size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x - size).put(y - size).put(z - size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x + size).put(y - size).put(z - size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

        vcBuffer.put(x + size).put(y - size).put(z + size); // v
        vcBuffer.put(Math.abs(normal.getX())).put(Math.abs(normal.getY())).put(Math.abs(normal.getZ())); // c

    }

}
