package voxel;

/**
 * Created by marcusbrooks on 10/02/15.
 */
public class Voxel {
    private boolean isActive;
    private Type type;

    public enum Type {
        EMPTY(0),
        FILLED(1);

        private int ID;

        Type(int i){
            ID = i;
        }

        public int getID(){
            return ID;
        }
    }

    public Voxel(Type type){

        this.type = type;

        if(type == Type.FILLED){
            setActive(true);
        } else {
            setActive(false);
        }
    }

    public final boolean isActive(){
        return isActive;
    }

    public void setActive(final boolean active){
        isActive = active;
    }

    public final int getID(){
        return type.getID();
    }

}
