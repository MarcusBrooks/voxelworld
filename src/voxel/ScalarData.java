package voxel;

import main.Global;
import main.Main;
import util.PerlinNoise;
import util.Rand;
import util.Util;

import java.util.Random;

/**
 * Created by marcusbrooks on 10/02/15.
 */
public class ScalarData {

    public static int currentFilledVoxels = 0;
    public static int currentEmptyVoxels = 0;


    public static Voxel[][][] createSphere() {
        Voxel[][][] chunk = new Voxel[Global.CHUNK_SIZE][Global.CHUNK_SIZE][Global.CHUNK_SIZE];
        currentFilledVoxels = 0;
        currentEmptyVoxels = 0;
        for (int x = 0; x < Global.CHUNK_SIZE; x++) {
            for (int y = 0; y < Global.CHUNK_SIZE; y++) {
                for (int z = 0; z < Global.CHUNK_SIZE; z++) {
                    if(x > 1 && x < Global.CHUNK_SIZE - 2 && y > 1 && y < Global.CHUNK_SIZE - 2 && z > 1 && z < Global.CHUNK_SIZE - 2) {
                        if (Math.sqrt((double) (x-1 - ((Global.CHUNK_SIZE-3) / 2)-1) * (x-1 - ((Global.CHUNK_SIZE-3) / 2)-1) +
                                                (y-1 - ((Global.CHUNK_SIZE-3) / 2)-1) * (y-1 - ((Global.CHUNK_SIZE-3) / 2)-1) +
                                                (z-1 - ((Global.CHUNK_SIZE-3) / 2)-1) * (z-1 - ((Global.CHUNK_SIZE-3) / 2))-1) <= (Global.CHUNK_SIZE-2) / 2) {

                            chunk[x][y][z] = new Voxel(Voxel.Type.FILLED);
                            chunk[x][y][z].setActive(true);
                            currentFilledVoxels++;
                        } else {
                            chunk[x][y][z] = new Voxel(Voxel.Type.EMPTY);
                            chunk[x][y][z].setActive(false);
                            currentEmptyVoxels++;
                        }
                    } else {
                        chunk[x][y][z] = new Voxel(Voxel.Type.EMPTY);
                        chunk[x][y][z].setActive(false);
                        currentEmptyVoxels++;
                    }
                }
            }
        }
        return chunk;
    }

    public static Voxel[][][] modifyChunk(Voxel[][][] chunk){

        for (int x = 0; x < Global.CHUNK_SIZE; x++) {
            for (int y = 0; y < Global.CHUNK_SIZE; y++) {
                for (int z = 0; z < Global.CHUNK_SIZE; z++) {
                    if(Math.sqrt((double) (x-1 - ((Global.CHUNK_SIZE-3) / 2)-1) * (x-1 - ((Global.CHUNK_SIZE-3) / 2)-1) +
                            (y-1 - ((Global.CHUNK_SIZE-3) / 2)-1) * (y-1 - ((Global.CHUNK_SIZE-3) / 2)-1) +
                            (z-1 - ((Global.CHUNK_SIZE-3) / 2)-1) * (z-1 - ((Global.CHUNK_SIZE-3) / 2))-1) <= 20) {

                        if(chunk[x][y][z].isActive()){
                            currentFilledVoxels++;
                            currentEmptyVoxels--;
                            chunk[x][y][z] = new Voxel(Voxel.Type.EMPTY);
                            chunk[x][y][z].setActive(false);
                        }
                    }
                }
            }
        }

         return chunk;
    }

    public static Voxel[][][] modifyChunk(Voxel[][][] chunk, int x1, int y2, int z3){
        int size = 6;
        for (int x = 0; x < Global.CHUNK_SIZE; x++) {
            for (int y = 0; y < Global.CHUNK_SIZE; y++) {
                for (int z = 0; z < Global.CHUNK_SIZE; z++) {
                    if(Math.sqrt((x-x1)*(x-x1) + (y-y2)*(y-y2) + (z-z3)*(z-z3)) <= size) {

                        if(chunk[x][y][z].isActive()){
                            currentFilledVoxels++;
                            currentEmptyVoxels--;
                            chunk[x][y][z] = new Voxel(Voxel.Type.EMPTY);
                            chunk[x][y][z].setActive(false);
                        }
                    }
                }
            }
        }

        return chunk;
    }

    public static Voxel[][][] createSpheres(int iter) {
        Random rand = new Random();
        Voxel[][][] chunk = new Voxel[Global.CHUNK_SIZE][Global.CHUNK_SIZE][Global.CHUNK_SIZE];

        for (int x = 0; x < Global.CHUNK_SIZE; x++) {
            for (int y = 0; y < Global.CHUNK_SIZE; y++) {
                for (int z = 0; z < Global.CHUNK_SIZE; z++) {
                    chunk[x][y][z] = new Voxel(Voxel.Type.EMPTY);
                    chunk[x][y][z].setActive(false);
                    currentEmptyVoxels++;
                }
            }
        }

        for (int i = 0; i < iter; i++) {
            int x1 = rand.nextInt(Global.CHUNK_SIZE);
            int y1 = rand.nextInt(Global.CHUNK_SIZE);
            int z1 = rand.nextInt(Global.CHUNK_SIZE);
            int size = 1;
            for (int x = 0; x < Global.CHUNK_SIZE; x++) {
                for (int y = 0; y < Global.CHUNK_SIZE; y++) {
                    for (int z = 0; z < Global.CHUNK_SIZE; z++) {

                        if(x < 2 || y < 2 || z < 2 || x > Global.CHUNK_SIZE - 2 ||  y > Global.CHUNK_SIZE - 2||  z > Global.CHUNK_SIZE - 2 ) {

                        } else if (Math.sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1) + (z - z1) * (z - z1)) <= size) {
                            if (!chunk[x][y][z].isActive()) {
                                currentFilledVoxels++;
                                currentEmptyVoxels--;
                                chunk[x][y][z] = new Voxel(Voxel.Type.FILLED);
                                chunk[x][y][z].setActive(true);
                            }
                        }
                    }
                }

            }
        }
        return chunk;
    }

    public static Voxel[][][] modifySurface(Voxel[][][] chunk, int x1, int z1) {
        int size = 1;
        for (int x = 0; x < Global.CHUNK_SIZE; x++) {
            for (int z = 0; z < Global.CHUNK_SIZE; z++) {
                boolean set = false;
                int y1 = 0;
                for (int y = Global.CHUNK_SIZE - 1; y >= 0; y--) {
                    if (chunk[x][y][z].isActive() && !set) {
                        y1 = y;
                        set = true;
                    }

                    if (set) {
                        if (Math.sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1) + (z - z1) * (z - z1)) <= size) {

                            if (chunk[x][y][z].isActive()) {
                                currentFilledVoxels++;
                                currentEmptyVoxels--;
                                chunk[x][y][z] = new Voxel(Voxel.Type.EMPTY);
                                chunk[x][y][z].setActive(false);
                            }
                        }
                    }
                }
            }
        }

        return chunk;

    }


    public static Voxel[][][] createPerlinNoise() {
        Voxel[][][] chunk = new Voxel[Global.CHUNK_SIZE][Global.CHUNK_SIZE][Global.CHUNK_SIZE];
        PerlinNoise perlin = new PerlinNoise();
        currentFilledVoxels = 0;
        currentEmptyVoxels = 0;
        int octave = 4;
        for (int x = 0; x < Global.CHUNK_SIZE; x++) {
            for (int z = 0; z < Global.CHUNK_SIZE; z++) {
                for (int y = 0; y < Global.CHUNK_SIZE; y++) {
                    if(x < 2 || y < 2 || z < 2 || x > Global.CHUNK_SIZE - 2 ||  y > Global.CHUNK_SIZE - 2||  z > Global.CHUNK_SIZE - 2 ) {
                        chunk[x][y][z] = new Voxel(Voxel.Type.EMPTY);
                        chunk[x][y][z].setActive(false);
                        currentEmptyVoxels++;
                    } else {
                        if (perlin.noise3(octave * x / (float) Global.CHUNK_SIZE, octave * y / (float) Global.CHUNK_SIZE, octave * z / (float) Global.CHUNK_SIZE) > 0) {
                            chunk[x][y][z] = new Voxel(Voxel.Type.FILLED);
                            chunk[x][y][z].setActive(true);
                            currentFilledVoxels++;
                        } else {
                            chunk[x][y][z] = new Voxel(Voxel.Type.EMPTY);
                            chunk[x][y][z].setActive(false);
                            currentEmptyVoxels++;
                        }

                    }
                }
            }
        }
        return chunk;
    }

    public static Voxel[][][] createAsteroid() {
        Voxel[][][] chunk = new Voxel[Global.CHUNK_SIZE][Global.CHUNK_SIZE][Global.CHUNK_SIZE];
        PerlinNoise perlin = new PerlinNoise();
        currentFilledVoxels = 0;
        currentEmptyVoxels = 0;

        int octave = 8;
        for (int x = 0; x < Global.CHUNK_SIZE; x++) {
            for (int y = 0; y < Global.CHUNK_SIZE; y++) {
                for (int z = 0; z < Global.CHUNK_SIZE; z++) {
                    if(x > 1 && x < Global.CHUNK_SIZE - 1 && y > 1 && y < Global.CHUNK_SIZE - 1 && z > 1 && z < Global.CHUNK_SIZE - 1) {
                        if (Math.sqrt((double) (x-1 - (Global.CHUNK_SIZE-3) / 2) * (x-1 - (Global.CHUNK_SIZE-3) / 2) +
                                                (y-1 - (Global.CHUNK_SIZE-3) / 2) * (y-1 - (Global.CHUNK_SIZE-3) / 2) +
                                                (z-1 - (Global.CHUNK_SIZE-3) / 2) * (z-1 - (Global.CHUNK_SIZE-3) / 2))
                                - (perlin.noise3(octave * x / (float) Global.CHUNK_SIZE, octave * y / (float) Global.CHUNK_SIZE, octave * z / (float) Global.CHUNK_SIZE))* 2  <= (Global.CHUNK_SIZE / 2) - 2){

                            chunk[x][y][z] = new Voxel(Voxel.Type.FILLED);
                            chunk[x][y][z].setActive(true);
                            currentFilledVoxels++;
                        } else {
                            chunk[x][y][z] = new Voxel(Voxel.Type.EMPTY);
                            chunk[x][y][z].setActive(false);
                            currentEmptyVoxels++;
                        }
                    } else {
                        chunk[x][y][z] = new Voxel(Voxel.Type.EMPTY);
                        chunk[x][y][z].setActive(false);
                        currentEmptyVoxels++;
                    }
                }
            }
        }
        return chunk;
        }

    public static Voxel[][][] createHeightMap() {
        Voxel[][][] chunk = new Voxel[Global.CHUNK_SIZE][Global.CHUNK_SIZE][Global.CHUNK_SIZE];
        double[][] heightmap = Util.createNewHeightMap();
        currentFilledVoxels = 0;
        currentEmptyVoxels = 0;
        for (int x = 0; x < Global.CHUNK_SIZE; x++) {
                for (int z = 0; z < Global.CHUNK_SIZE; z++) {
                    for (int y = 0; y < Global.CHUNK_SIZE; y++) {
                        if(x < 2 || y < 2 || z < 2 || x > Global.CHUNK_SIZE - 2 ||  y > Global.CHUNK_SIZE - 2||  z > Global.CHUNK_SIZE - 2 ){
                            chunk[x][y][z] = new Voxel(Voxel.Type.EMPTY);
                            chunk[x][y][z].setActive(false);
                            currentEmptyVoxels++;
                        } else if(y < (heightmap[x][z]+1)*Global.CHUNK_SIZE/2){
                            chunk[x][y][z] = new Voxel(Voxel.Type.FILLED);
                            chunk[x][y][z].setActive(true);
                            currentFilledVoxels++;
                        } else {
                            chunk[x][y][z] = new Voxel(Voxel.Type.EMPTY);
                            chunk[x][y][z].setActive(false);
                            currentEmptyVoxels++;
                        }
                }
            }
        }
            return chunk;
    }

    public static Voxel[][][] createSineWaves() {
        Voxel[][][] chunk = new Voxel[Global.CHUNK_SIZE][Global.CHUNK_SIZE][Global.CHUNK_SIZE];
        currentFilledVoxels = 0;
        currentEmptyVoxels = 0;
        int scaling = Global.CHUNK_SIZE / 1;
        for (int x = 0; x < Global.CHUNK_SIZE; x++) {
            for (int y = 0; y < Global.CHUNK_SIZE; y++) {
                for (int z = 0; z < Global.CHUNK_SIZE; z++) {
                    if (Math.sin((Global.CHUNK_SIZE / (scaling * Math.PI) * x)) +
                            Math.sin((Global.CHUNK_SIZE / (scaling * Math.PI) * y)) +
                            Math.sin((Global.CHUNK_SIZE / (scaling * Math.PI) * z)) <= 0) {

                        chunk[x][y][z] = new Voxel(Voxel.Type.FILLED);
                        chunk[x][y][z].setActive(true);
                        currentFilledVoxels++;
                    } else {
                        chunk[x][y][z] = new Voxel(Voxel.Type.EMPTY);
                        chunk[x][y][z].setActive(false);
                        currentEmptyVoxels++;
                    }
                }
            }
        }
        return chunk;
    }



    public static Voxel[][][] createCube(){
        Voxel[][][] chunk = new Voxel[Global.CHUNK_SIZE][Global.CHUNK_SIZE][Global.CHUNK_SIZE];
        currentFilledVoxels = 0;
        currentEmptyVoxels = 0;
        for (int x = 0; x < Global.CHUNK_SIZE; x++) {
            for (int y = 0; y < Global.CHUNK_SIZE; y++) {
                for (int z = 0; z < Global.CHUNK_SIZE; z++) {
                    if (x > 2 && y > 2 && z > 2 && x < Global.CHUNK_SIZE - 3 && y < Global.CHUNK_SIZE - 3 && z < Global.CHUNK_SIZE - 3) {
                        chunk[x][y][z] = new Voxel(Voxel.Type.FILLED);
                        chunk[x][y][z].setActive(true);
                        currentFilledVoxels++;
                    } else {
                        chunk[x][y][z] = new Voxel(Voxel.Type.EMPTY);
                        chunk[x][y][z].setActive(false);
                        currentEmptyVoxels++;
                    }
                }
            }
        }
        return chunk;
    }


}

