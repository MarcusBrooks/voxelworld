package voxel;

import org.lwjgl.util.vector.Vector3f;

/**
 * Created by marcusbrooks on 04/03/15.
 */
public class Node {

    byte edgeMask;
    byte quadMask;

    Vector3f pos;
    Vector3f newPos;



    public Node(int x, int y, int z){
        edgeMask = 0x00;
        quadMask = 0x00;
        pos = new Vector3f(x, y, z);
    }

    public void setEdgeMask(byte mask){
        this.edgeMask = mask;
    }

    public void setQuadMask(byte mask) {
        this.quadMask = mask;
    }

    public byte getEdgeMask() {
        return edgeMask;
    }

    public byte getQuadMask() {
        return quadMask;
    }

    public Vector3f getPosition(){
        return pos;
    }

    public void setPosition(final Vector3f newPos){
        this.newPos = newPos;
    }

    public void updatePosition(){
        this.pos = this.newPos;
    }

    public void setPosition2(final Vector3f pos){
        this.pos = pos;
    }

}