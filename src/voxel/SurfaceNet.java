package voxel;

import main.Global;
import main.Main;
import util.Util;
import org.lwjgl.util.vector.Vector3f;

import java.nio.FloatBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by marcusbrooks on 27/02/15.
 */
public class SurfaceNet {
    private static final Logger debug = Logger.getLogger(Main.class.getName());

    private static byte[] edgeTable;
    private static byte[] quadTable;

    private static int[] edgeMask = {0x08, 0xF0, 0x33, 0xCC, 0x99, 0x66};
    private static int[] quadMask = {0x03, 0x05, 0x11};
    private static Node[][][] surfaceNodes;

    public static void initTables() {

        initEdgeTable();
        initQuadTable();

    }

    private static void initQuadTable() {

    }

    private static void initEdgeTable() {
        long start = System.nanoTime();
        edgeTable = new byte[256];
        quadTable = new byte[256];

        for (int i = 0; i < 256; ++i) {
            byte em = 0;

            for (int j = 0; j < 6; j++) {
                if (((i & edgeMask[j])) != 0x00 && ((i & edgeMask[j])) != edgeMask[j]) {
                    em |= 1 << j;
                }
            }
            edgeTable[i] = em;
        }
        for (int i = 0; i < 256; ++i) {
            byte qm = 0;
            for (int j = 0; j < 3; j++) {
                if ((i & quadMask[j]) != 0x00 && (i & quadMask[j]) != quadMask[j]) {
                    qm |= 1 << j;
                    if((i & quadMask[j]) == 0x01){
                        qm |= 1 << (j+3);
                    } else {
                    }

                }
            }
            quadTable[i] = qm;
        }

        long end = System.nanoTime();
        debug.log(Level.INFO, "SurfaceNet Edge Table Calculation time - " + (float) (end - start) / 1e6 + "ms");
    }

    public static void meshSurfaceNodes(FloatBuffer vcBuffer) {

        int[] quadCount = new int[6];
        for (int i = 0; i < 6; i++) {
            quadCount[i] = 0;
        }
        for (int x = 1; x < Global.CHUNK_SIZE - 1; x++) {
            for (int y = 1; y < Global.CHUNK_SIZE - 1; y++) {
                for (int z = 1; z < Global.CHUNK_SIZE - 1; z++) {

                    if(surfaceNodes == null || surfaceNodes[x][y][z] == null){
                        continue;
                    }

                    byte quads = surfaceNodes[x][y][z].getQuadMask();
                    
                    if (quads == 0x00) { //If no quads are connected to this vertex
                        continue;
                    }

                    for (int i = 0; i < 3; i++) { //Loops through each of the quad starting edges
                        if ((quads & (1 << i)) != 0x00) { //If edge is connected

                            if(i == 0){

                                if((quads & 1 << 3) != 0x00 || Global.USE_SIMPLE_RENDERING){
                                    quadCount[0]++;
                                    Vector3f color = Util.getColor(surfaceNodes[x][y][z].getPosition(), surfaceNodes[x - 1][y][z].getPosition(),surfaceNodes[x-1][y-1][z].getPosition(),surfaceNodes[x][y-1][z].getPosition(),  i + 3);

                                    vcBuffer.put(surfaceNodes[x][y][z].getPosition().getX()).put(surfaceNodes[x][y][z].getPosition().getY()).put(surfaceNodes[x][y][z].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x-1][y][z].getPosition().getX()).put(surfaceNodes[x-1][y][z].getPosition().getY()).put(surfaceNodes[x-1][y][z].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x-1][y-1][z].getPosition().getX()).put(surfaceNodes[x-1][y-1][z].getPosition().getY()).put(surfaceNodes[x-1][y-1][z].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x][y-1][z].getPosition().getX()).put(surfaceNodes[x][y-1][z].getPosition().getY()).put(surfaceNodes[x][y-1][z].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                } else {

                                    quadCount[1]++;
                                    Vector3f color = Util.getColor( surfaceNodes[x][y][z].getPosition(),surfaceNodes[x][y-1][z].getPosition(), surfaceNodes[x-1][y-1][z].getPosition(),surfaceNodes[x-1][y][z].getPosition(), i);

                                    vcBuffer.put(surfaceNodes[x][y][z].getPosition().getX()).put(surfaceNodes[x][y][z].getPosition().getY()).put(surfaceNodes[x][y][z].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x][y-1][z].getPosition().getX()).put(surfaceNodes[x][y-1][z].getPosition().getY()).put(surfaceNodes[x][y-1][z].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x-1][y-1][z].getPosition().getX()).put(surfaceNodes[x-1][y-1][z].getPosition().getY()).put(surfaceNodes[x-1][y-1][z].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x-1][y][z].getPosition().getX()).put(surfaceNodes[x-1][y][z].getPosition().getY()).put(surfaceNodes[x-1][y][z].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                }
                            } else if(i == 1){
                                if((quads & 1 << 4) != 0x00 || Global.USE_SIMPLE_RENDERING){
                                    quadCount[2]++;
                                    Vector3f color = Util.getColor(surfaceNodes[x][y][z].getPosition(), surfaceNodes[x][y][z-1].getPosition(), surfaceNodes[x-1][y][z-1].getPosition(), surfaceNodes[x-1][y][z].getPosition(), i+3);
                                    vcBuffer.put(surfaceNodes[x][y][z].getPosition().getX()).put(surfaceNodes[x][y][z].getPosition().getY()).put(surfaceNodes[x][y][z].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x][y][z-1].getPosition().getX()).put(surfaceNodes[x][y][z-1].getPosition().getY()).put(surfaceNodes[x][y][z-1].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x-1][y][z-1].getPosition().getX()).put(surfaceNodes[x-1][y][z-1].getPosition().getY()).put(surfaceNodes[x-1][y][z-1].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x-1][y][z].getPosition().getX()).put(surfaceNodes[x - 1][y][z].getPosition().getY()).put(surfaceNodes[x-1][y][z].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                } else {
                                    quadCount[3]++;
                                    Vector3f color = Util.getColor(surfaceNodes[x][y][z].getPosition(), surfaceNodes[x-1][y][z].getPosition(), surfaceNodes[x-1][y][z-1].getPosition(),surfaceNodes[x][y][z-1].getPosition(), i);
                                    vcBuffer.put(surfaceNodes[x][y][z].getPosition().getX()).put(surfaceNodes[x][y][z].getPosition().getY()).put(surfaceNodes[x][y][z].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x-1][y][z].getPosition().getX()).put(surfaceNodes[x-1][y][z].getPosition().getY()).put(surfaceNodes[x-1][y][z].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x-1][y][z-1].getPosition().getX()).put(surfaceNodes[x-1][y][z-1].getPosition().getY()).put(surfaceNodes[x-1][y][z-1].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x][y][z-1].getPosition().getX()).put(surfaceNodes[x][y][z-1].getPosition().getY()).put(surfaceNodes[x][y][z-1].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                }
                            } else if(i == 2){
                                if((quads & 1 << 5) != 0x00 || Global.USE_SIMPLE_RENDERING){
                                    quadCount[4]++;
                                    Vector3f color = Util.getColor(surfaceNodes[x][y][z].getPosition(), surfaceNodes[x][y-1][z].getPosition(), surfaceNodes[x][y-1][z-1].getPosition(), surfaceNodes[x][y][z-1].getPosition(), i+3);
                                    vcBuffer.put(surfaceNodes[x][y][z].getPosition().getX()).put(surfaceNodes[x][y][z].getPosition().getY()).put(surfaceNodes[x][y][z].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x][y-1][z].getPosition().getX()).put(surfaceNodes[x][y-1][z].getPosition().getY()).put(surfaceNodes[x][y-1][z].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x][y-1][z-1].getPosition().getX()).put(surfaceNodes[x][y-1][z-1].getPosition().getY()).put(surfaceNodes[x][y-1][z-1].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x][y][z-1].getPosition().getX()).put(surfaceNodes[x][y][z-1].getPosition().getY()).put(surfaceNodes[x][y][z-1].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                } else {
                                    quadCount[5]++;
                                    Vector3f color = Util.getColor(surfaceNodes[x][y][z].getPosition(), surfaceNodes[x][y][z-1].getPosition(), surfaceNodes[x][y-1][z-1].getPosition(),surfaceNodes[x][y-1][z].getPosition(), i);

                                    vcBuffer.put(surfaceNodes[x][y][z].getPosition().getX()).put(surfaceNodes[x][y][z].getPosition().getY()).put(surfaceNodes[x][y][z].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x][y][z-1].getPosition().getX()).put(surfaceNodes[x][y][z-1].getPosition().getY()).put(surfaceNodes[x][y][z-1].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x][y-1][z-1].getPosition().getX()).put(surfaceNodes[x][y-1][z-1].getPosition().getY()).put(surfaceNodes[x][y-1][z-1].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());
                                    vcBuffer.put(surfaceNodes[x][y-1][z].getPosition().getX()).put(surfaceNodes[x][y-1][z].getPosition().getY()).put(surfaceNodes[x][y-1][z].getPosition().getZ());
                                    vcBuffer.put(color.getX()).put(color.getY()).put(color.getZ());

                                }
                            }
                        }
                    }
                }
            }
        }

        int totalQuads = 0;
        for (int i = 0; i < 6; i++) {
            System.out.println((i+1) + ": " + quadCount[i]);
            totalQuads += quadCount[i];
        }
        debug.log(Level.INFO, "Number of quads rendered - " + totalQuads);
    }

    public static void smoothSurfaceByEdgeMidpoints(int iter){
        long start = System.nanoTime();

        for (int i = 0; i < iter; i++) {


            for (int x = 1; x < Global.CHUNK_SIZE - 2; x++) {
                for (int y = 1; y < Global.CHUNK_SIZE - 2; y++) {
                    for (int z = 1; z < Global.CHUNK_SIZE - 2; z++) {

                        if (surfaceNodes[x][y][z] == null) {
                            continue;
                        }

                        Vector3f pos = surfaceNodes[x][y][z].getPosition();
                        byte edgeMask = surfaceNodes[x][y][z].getEdgeMask();
                        Vector3f newPos = null;

                        for (int j = 0; j < 6; j++) {
                            if ((edgeMask & (1 << j)) != 0x00) {

                                Node node;

                                //Get the position of each adjacent surface node
                                if (j == 0) {
                                    node = surfaceNodes[x - 1][y][z];
                                } else if (j == 1) {
                                    node = surfaceNodes[x + 1][y][z];
                                } else if (j == 2) {
                                    node = surfaceNodes[x][y - 1][z];
                                } else if (j == 3) {
                                    node = surfaceNodes[x][y + 1][z];
                                } else if (j == 4) {
                                    node = surfaceNodes[x][y][z - 1];
                                } else if (j == 5) {
                                    node = surfaceNodes[x][y][z + 1];
                                } else {
                                    node = null;
                                }

                                if (node != null) {
                                    Vector3f nodePos = node.getPosition();
                                    Vector3f edgeCenter = new Vector3f((pos.x + nodePos.x) / 2, (pos.y + nodePos.y) / 2, (pos.z + nodePos.z) / 2);

                                    if (newPos == null) {
                                        newPos = new Vector3f(edgeCenter);
                                    } else {
                                        newPos.set((newPos.x + edgeCenter.x) / 2, (newPos.y + edgeCenter.y) / 2, (newPos.z + edgeCenter.z) / 2);
                                    }
                                }
                            }
                        }
                        surfaceNodes[x][y][z].setPosition2(newPos);

                    }
                }
            }

        }

        long end = System.nanoTime();
        debug.log(Level.INFO, "SurfaceNet Edge Smoothing, " + iter + " iterations - " + (float) (end - start) / 1e6 + "ms");
    }

    public static void smoothSurfaceIteratively(int iter){
        long start = System.nanoTime();

        for (int i = 0; i < iter; i++) {

            for (int x = 1; x < Global.CHUNK_SIZE - 2; x++) {
                for (int y = 1; y < Global.CHUNK_SIZE - 2; y++) {
                    for (int z = 1; z < Global.CHUNK_SIZE - 2; z++) {

                        if (surfaceNodes[x][y][z] == null) {
                            continue;
                        }

                        Vector3f newPos = null;
                        for (int j = 0; j < 6; j++) {
                            Node node;

                            //Get the position of each adjacent surface node
                            if (j == 0) {
                                node = surfaceNodes[x - 1][y][z];
                            } else if (j == 1) {
                                node = surfaceNodes[x + 1][y][z];
                            } else if (j == 2) {
                                node = surfaceNodes[x][y - 1][z];
                            } else if (j == 3) {
                                node = surfaceNodes[x][y + 1][z];
                            } else if (j == 4) {
                                node = surfaceNodes[x][y][z - 1];
                            } else if (j == 5) {
                                node = surfaceNodes[x][y][z + 1];
                            } else {
                                node = null;
                            }

                            if(node != null) {
                                if (newPos == null) {
                                    newPos = node.getPosition();
                                } else {
                                    newPos.set((newPos.getX() + node.getPosition().getX()) / 2,
                                            (newPos.getY() + node.getPosition().getY()) / 2,
                                            (newPos.getZ() + node.getPosition().getZ()) / 2);
                                }
                            }

                        }


                        surfaceNodes[x][y][z].setPosition(newPos);
                    }
                }
            }

            for (int x = 1; x < Global.CHUNK_SIZE - 2; x++) {
                for (int y = 1; y < Global.CHUNK_SIZE - 2; y++) {
                    for (int z = 1; z < Global.CHUNK_SIZE - 2; z++) {
                        Node node = surfaceNodes[x][y][z];

                        if (node != null) {
                            node.updatePosition();
                        }
                    }
                }
            }

        }
        long end = System.nanoTime();
        debug.log(Level.INFO, "SurfaceNet smoothing time - " + (float) (end - start) / 1e6 + "ms");

    }

    /**
     * @param voxels - voxel data
     * @param size   - the chunk size
     * @return
     */
    public static Node[][][] createSurfaceNodes(Voxel[][][] voxels, int size) {
        long start = System.nanoTime();

        surfaceNodes = new Node[size - 1][size - 1][size - 1];

        for (int x = 0; x < size - 1; x++) {
            for (int y = 0; y < size - 1; y++) {
                for (int z = 0; z < size - 1; z++) {

                    Voxel[][][] chunk = new Voxel[2][2][2];

                    for (int x2 = 0; x2 < 2; x2++) {
                        for (int y2 = 0; y2 < 2; y2++) {
                            for (int z2 = 0; z2 < 2; z2++) {
                                chunk[x2][y2][z2] = voxels[x + x2][y + y2][z + z2];
                            }
                        }
                    }
                    int mask = checkSurfaceNode(chunk);

                    if (mask == 0x00 || mask == 0xff) {
                        surfaceNodes[x][y][z] = null; //Init every cell as an empty node
                        continue; //If mask lies completely outside, or completely inside the voxel grid
                    }
                    surfaceNodes[x][y][z] = new Node(x, y, z); //Init every cell as an empty node

                    byte edgeMask = edgeTable[mask];

                    byte snQuadMask;
                    if(Global.USE_PRECALC_QUAD_TABLE) {
                        snQuadMask = quadTable[mask];
                    } else {
                        byte qm = 0;
                        for (int j = 0; j < 3; j++) {
                            if ((mask & quadMask[j]) != 0x00 && (mask & quadMask[j]) != quadMask[j]) {
                                qm |= 1 << j;
                                if ((mask & quadMask[j]) == 0x01) {
                                    qm |= 1 << (j + 3);
                                } else {
                                }

                            }
                        }
                        snQuadMask = qm;
                    }

                    surfaceNodes[x][y][z].setEdgeMask(edgeMask); //Node lies on surface
                    surfaceNodes[x][y][z].setQuadMask(snQuadMask);
                }
            }
        }

        long end = System.nanoTime();
        debug.log(Level.INFO, "Surface Node Calculation time - " + (float) (end - start) / 1e6 + "ms");

        return surfaceNodes;

    }

    public static int checkSurfaceNode(Voxel[][][] voxels) {
        byte mask = 0x00;
        int g = 0;
        for (int x = 0; x < 2; x++) {
            for (int y = 0; y < 2; y++) {
                for (int z = 0; z < 2; z++, g++) {
                    mask |= voxels[x][y][z].isActive() ? (0x01 << g) : 0x00; //If voxel exists, then binary OR the position into the mask
                }
            }
        }
        return mask & 0xFF; //Flip bits to unsigned
    }

}

